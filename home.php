<?php 
$paged = get_query_var( 'paged', 1 );
?>

<?php get_header(); ?>

<?php nectar_page_header(get_option('page_for_posts')); ?>

<div class="container-wrap">
    
  <div class="container main-content">
    
    <div class="row">
      <h1>News, Updates & Accomplishments</h1>
      <?php $options = get_nectar_theme_options(); 
      
      $blog_type = $options['blog_type'];
      if($blog_type == null) $blog_type = 'std-blog-sidebar';
      
      $masonry_class = null;
      $masonry_style = null;
      $infinite_scroll_class = null;
      $load_in_animation = (!empty($options['blog_loading_animation'])) ? $options['blog_loading_animation'] : 'none';

      //enqueue masonry script if selected
      if($blog_type == 'masonry-blog-sidebar' || $blog_type == 'masonry-blog-fullwidth' || $blog_type == 'masonry-blog-full-screen-width') {
        $masonry_class = 'masonry';
      }
      
      if($blog_type == 'masonry-blog-full-screen-width') {
        $masonry_class = 'masonry full-width-content';
      }
      
      if(!empty($options['blog_pagination_type']) && $options['blog_pagination_type'] == 'infinite_scroll'){
        $infinite_scroll_class = ' infinite_scroll';
      }

      if($masonry_class != null) {
        $masonry_style = (!empty($options['blog_masonry_type'])) ? $options['blog_masonry_type']: 'classic';
      }
      
      echo '<div id="post-area" class="col span_12 col_last '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'"> <div class="posts-container"  data-load-animation="'.$load_in_animation.'">';
    
        $args = array(
          'post_type' => 'post',
          'paged' => get_query_var('paged'),
          'cat' => '1' // Only show posts categorized as "News" (default category)
        );
        $news_query = new WP_Query($args);

        if( $news_query->have_posts() ) :
          while( $news_query->have_posts() ) :
            $news_query->the_post();
             get_template_part( 'includes/post-templates/entry', get_post_format() ); 
          endwhile;
        else: ?>
          <h3>Nothing to show right now.</h3>
        <?php endif;
        wp_reset_postdata(); ?>
        
      <?php nectar_pagination(); ?>
        
        </div> <!-- /.posts-container -->
      </div> <!-- /#post-area -->
    </div> <!-- /row -->

  
    <?php
    // 
    // Only show events and lectures on landing page, not paginated
    // 
    if ( $paged == 1 ): ?>
      

    <!-- Events -->
    <div class="row">
      <h1>Events</h1>
      <?php

      echo '<div id="post-area" class="col span_12 col_last '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'"> <div class="posts-container"  data-load-animation="'.$load_in_animation.'">';


      $num_posts = 6;
      $args = array(
        'posts_per_page' => $num_posts / 2,
        'post_type' => 'post',
        'cat' => 6 // 6 is the Events Category term id
      );
      $events_query = new WP_Query($args);
      $event_post_num = 0;
      if( $events_query->have_posts() ) : while( $events_query->have_posts() ) : $events_query->the_post();
        $event_post_num++;

        if( $event_post_num <= $num_posts / 2 )
           get_template_part( 'includes/post-templates/entry', get_post_format() ); 
         
      endwhile; endif;
      ?>
        </div> <!-- /.posts-container -->
        <h3 class="events-and-lectures-more-link"><a href="/category/event/">See All Events</a> </h3>        
      </div> <!-- /#post-area -->
    </div> <!-- /row -->


    <!-- Lecture Series -->
    <div class="row">
      <h1>Lecture Series</h1>
      <?php

      echo '<div id="post-area" class="col span_12 col_last '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'"> <div class="posts-container"  data-load-animation="'.$load_in_animation.'">';

      // 
      // Lectures
      // 
      $args = array(
        'posts_per_page' => $num_posts / 2,
        'post_type' => 'post',
        'cat' => 7 // 7 is the Lecture Category term id
      );
      $events_query = new WP_Query($args);
      $event_post_num = 0;
      if( $events_query->have_posts() ) : while( $events_query->have_posts() ) : $events_query->the_post();
        $event_post_num++;

        if( $event_post_num <= $num_posts / 2 )
           get_template_part( 'includes/post-templates/entry', get_post_format() ); 
         
      endwhile; endif;
      ?>
        </div> <!-- /.posts-container -->
        <h3 class="events-and-lectures-more-link"> <a href="/category/lecture/">See All Lectures</a> </h3>
      </div> <!-- /#post-area -->
    </div> <!-- /row -->

    <?php endif; //end if !$wp_query->paged ?>

  </div><!--/container-->

</div><!--/container-wrap-->
  
<?php get_footer(); ?>