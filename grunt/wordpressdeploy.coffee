module.exports =
  options:
    # backups_dir: "backups"
    rsync_args: [
      "--verbose"
      "--progress"
      "-rlpt"
      "--compress"
      "--omit-dir-times"
      # "--delete"
      # "--dry-run"
    ]
    exclusions: [
      "Gruntfile.*"
      ".git/"
      "tmp/*"
      "backups/"
      "wp-config.php"
      "composer.json"
      "composer.lock"
      "README.md"
      ".gitignore"
      "package.json"
      "node_modules"
      ".DS_Store"
      "sftp-config.json"
      "codekit-config.json"
      "config.codekit"
      # "*.less"  # Comment this line if you compile LESS at runtime (but why?)
      "backwpup-*"
      "grunt"
      "vendor"
      "*.coffee"
      "deployconfig.*"
      ".codekit-cache/"      
      # Grunt Build
      "releases/"
      "build/"
      # For Use with Roots Theme Setup
      "assets/less/"
      "assets/js/plugins"
      ".editorconfig" 
      ".bowerrc" 
      "bower.json"       
      # Theme-specific Exclusions
      "_client-files/" # An example of a folder you wouldn't want to sync
    ]

  local:
    title: "Local"
    database: "vccf"
    table_prefix: "wp_"
    table_exclusions : [
      "_rg",
      # "wfBlocks",
      # "wfBlocksAdv",
      # "wfFileMods",
      # "wfHits"
    ]
    user: "admin"
    pass: "admin"
    host: "localhost"
    url: "//vccf.dev"
    path:
      parent_theme   : "<%= deployconfig.local.wp_content_path %>/themes/salient-7.5.2/"
      theme   : "<%= deployconfig.local.wp_content_path %>/themes/<%= grunt.config.data.theme_name %>/build/"
      uploads : "<%= deployconfig.local.wp_content_path %>/uploads/"
      plugins : "<%= deployconfig.local.wp_content_path %>/plugins/"


  # ==========  Start Environment Definitions  ==========

  live:
    title: "Live Site"
    database: "vccfound_wp655"
    table_prefix: "wp_"
    table_exclusions : [
      "_wf" # Will exclude with " NOT LIKE '%_wf%' " SQL statement
    ]        
    user: "<%= deployconfig.live.db_user %>"
    pass: "<%= deployconfig.live.db_pass %>"
    host: "77.104.138.152"
    # port: 3309 # sometimes SQL needs a specific port
    url: "//vccfoundation.org"
    sql_remove: [
      "Warning: Using a password on the command line interface can be insecure."
    ]
    path:
      parent_theme : "<%= deployconfig.live.wp_content_path %>/themes/salient-7.5.2/"
      theme        : "<%= deployconfig.live.wp_content_path %>/themes/<%= grunt.config.data.theme_name %>/"
      uploads      : "<%= deployconfig.live.wp_content_path %>/uploads/"
      plugins      : "<%= deployconfig.live.wp_content_path %>/plugins/"
    ssh_host: "vccfound@us18.siteground.us -p18765"
  
  old:
    title: "Ported Old Site [Siteground]"
    database: "vccfound_old"
    table_prefix: "wp_"
    table_exclusions : [
      "_wf" # Will exclude with " NOT LIKE '%_wf%' " SQL statement
    ]        
    user: "<%= deployconfig.old.db_user %>"
    pass: "<%= deployconfig.old.db_pass %>"
    host: "77.104.138.152"
    # port: 3309 # sometimes SQL needs a specific port
    url: "//old.vccfoundation.org"
    sql_remove: [
      "Warning: Using a password on the command line interface can be insecure."
    ]    
    path:
      parent_theme : "<%= deployconfig.old.wp_content_path %>/themes/salient-7.5.2/"
      theme        : "<%= deployconfig.old.wp_content_path %>/themes/<%= grunt.config.data.theme_name %>/"
      uploads      : "<%= deployconfig.old.wp_content_path %>/uploads/"
      plugins      : "<%= deployconfig.old.wp_content_path %>/plugins/"
    ssh_host: "vccfound@us18.siteground.us -p18765"
  
  dev:
    title: "Dev Site"
    database: "vccfound_dev"
    table_prefix: "wp_"
    table_exclusions : [
      "_rg" # Will exclude with " NOT LIKE '%_wf%' " SQL statement
    ]        
    user: "<%= deployconfig.dev.db_user %>"
    pass: "<%= deployconfig.dev.db_pass %>"
    host: "77.104.138.152"
    # port: 3309 # sometimes SQL needs a specific port
    url: "//dev.vccfoundation.org"
    sql_remove: [
      "Warning: Using a password on the command line interface can be insecure."
    ]    
    path:
      parent_theme : "<%= deployconfig.dev.wp_content_path %>/themes/salient-7.5.2/"
      theme        : "<%= deployconfig.dev.wp_content_path %>/themes/<%= grunt.config.data.theme_name %>/"
      uploads      : "<%= deployconfig.dev.wp_content_path %>/uploads/"
      plugins      : "<%= deployconfig.dev.wp_content_path %>/plugins/"
    ssh_host: "vccfound@us18.siteground.us -p18765"
