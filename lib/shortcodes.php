<?php 

// Require all PHP files from the /shortcodes folder
$pattern = dirname( __FILE__ ) . "/shortcodes/*.php";
$shortcodes = glob( $pattern );

foreach ($shortcodes as $sc_file) {
  require_once $sc_file;
}
unset($sc_file, $sc_filepath);

?>
