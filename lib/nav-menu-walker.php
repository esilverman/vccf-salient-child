<?php
  function nectar_walker_nav_menu() {
    $i = 12;
    class Nectar_Arrow_Walker_Nav_Menu extends Walker_Nav_Menu {
      function __construct() {
        add_filter('nav_menu_css_class', array($this, 'css_classes'), 10, 2);
        add_filter('nav_menu_item_id', '__return_null');
        $cpt           = get_post_type();
        $this->cpt     = in_array($cpt, get_post_types(array('_builtin' => false)));
        $this->archive = get_post_type_archive_link($cpt);
      }
      
      function display_element($element, &$children_elements, $max_depth, $depth=0, $args, &$output) {
        $id_field = $this->db_fields['id'];
        global $options;
        $theme_skin = (!empty($options['theme-skin']) && $options['theme-skin'] == 'ascend') ? 'ascend' : 'default';

        //button styling
        $button_style = get_post_meta( $element->$id_field, 'menu-item-nectar-button-style', true);
        if(!empty($button_style))
          $element->classes[] = $button_style;

        if (!empty($children_elements[$element->$id_field]) && $element->menu_item_parent == 0 && $theme_skin !='ascend') { 
          $element->title =  $element->title . '<span class="sf-sub-indicator"><i class="icon-angle-down"></i></span>'; 
          $element->classes[] = 'sf-with-ul';
        }
        
        if (!empty($children_elements[$element->$id_field]) && $element->menu_item_parent != 0) { 
          $element->title =  $element->title . '<span class="sf-sub-indicator"><i class="icon-angle-right"></i></span>'; 
        }
        
        Walker_Nav_Menu::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
      } // end display_element
      public function css_classes($classes, $item) {
        $slug = sanitize_title($item->title);

        if ($this->cpt) {
          $classes = str_replace('current_page_parent', '', $classes);

          if (roots_url_compare($this->archive, $item->url)) {
            $classes[] = 'active';
          }
        }

        // $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
        $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

        $classes[] = 'menu-' . $slug;

        $classes = array_unique($classes);

        return array_filter($classes, 'is_element_empty');
      }

    }

  }
?>