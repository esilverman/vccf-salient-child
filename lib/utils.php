<?php

function is_element_empty($element) {
  $element = trim($element);
  return !empty($element);
}

/**
 * Shorthand for custom post type query
 * 
 * Accepts the following params:
 * Custom Post Type - (string) $cpt_name
 * Limit for number of posts - (int) $posts_per_page
 * [Optional] Array of args used in WP_Query
 */
if( !function_exists( 'cpt_query' ) ) {
  function cpt_query($cpt_name = 0, $args_or_posts_per_page = NULL) {
    if( !$cpt_name ) return false;

    // If we're passed an integer, it's to set posts_per_page.
    // Otherwise, assume it's an args array
    $args = gettype( $args_or_posts_per_page ) == 'integer'
              ? array('posts_per_page' => $args_or_posts_per_page)
              : $args_or_posts_per_page ;

    $defaults = array(
      'post_type' => $cpt_name,
      'posts_per_page' => -1
    );
    $args = wp_parse_args($args, $defaults);
    
    $cpt_query = new WP_Query($args);

    return $cpt_query;
  } // END cpt_query
} // END !function_exists( 'cpt_query' )

?>