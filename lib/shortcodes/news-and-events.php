<?php 

//Recent Posts
function news_and_events_feed($atts, $content = null) {
  extract(shortcode_atts(array(
    'num_posts' => '4',
    ), $atts));  
  
  global $post;  
    
  ob_start(); 
    
  ?>
  <div class="row">
      
    <?php
    $options = get_option('salient'); 
    
    $blog_type = $options['blog_type'];
    if($blog_type == null) $blog_type = 'std-blog-sidebar';
    
    $masonry_class = 'masonry';
    $masonry_style = 'classic';
    $infinite_scroll_class = null;

    // echo '<div id="post-area" class="col span_12 col_last '.$masonry_class.' '.$masonry_style.' '. $infinite_scroll_class.'">';
    
    ?>
    <div id="post-area" class="col span_12 col_last masonry classic_enhanced">
      <div class="posts-container">
        <div class="row">

          <?php
          $args = array(
            'posts_per_page' => $num_posts / 2,
            'post_type' => 'post',
            'category__not_in' => array(6) // 6 is the Events Category term id
          );
          $news_query = new WP_Query($args);
          $news_post_num = 0;
          $news_posts_on_page = array();
          if( $news_query->have_posts() ) : while( $news_query->have_posts() ) : $news_query->the_post();
            $news_post_num++;
            $news_posts_on_page[] = get_the_ID();

            if( $news_post_num <= $num_posts / 2 )
              get_template_part( 'includes/post-templates/entry-news-and-event-feed' );
             
          endwhile; endif; ?>


          <?php
          $args = array(
            'posts_per_page' => $num_posts / 2,
            'post_type' => 'post',
            'post__not_in' => $news_posts_on_page,
            'cat' => 6 // 6 is the Events Category term id
          );
          $events_query = new WP_Query($args);
          $event_post_num = 0;
          if( $events_query->have_posts() ) : while( $events_query->have_posts() ) : $events_query->the_post();
            $event_post_num++;

            if( $event_post_num <= $num_posts / 2 )
              get_template_part( 'includes/post-templates/entry-news-and-event-feed' );
             
          endwhile; endif; ?>
        </div> <!-- /row -->
      </div> <!-- /posts-container -->
    </div> <!-- /post-area -->

  </div> <!-- /row -->

  
  <?php

  wp_reset_query();
  
  $news_and_events_feed = ob_get_contents();
  
  ob_end_clean();
  
  return $news_and_events_feed;

}
add_shortcode('news_and_events', 'news_and_events_feed');

 ?>