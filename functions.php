<?php 

add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
}

/**
 * This is for things like CPT generation
 * where we need to namespace stuff
 */
define('THEME_PREFIX', 'vccf_');


/**
 * Bolt includes
 *
 * The $bolt_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 */
$bolt_includes = array(
  'lib/utils.php',                  // Helper Functions
  'lib/init.php',                   // Initial theme setup and constants
  // 'lib/logging.php',                // Debugging / Kenny loggins stuff
  'lib/scripts-styles.php',         // JS & CSS Enqueuing for WP
  'lib/nav-menu-walker.php',        // Custom Nav Walker
  // 'lib/custom-post-types.php',      // Define Custom Post Types
  // 'lib/custom-taxonomies.php',      // Define Custom Taxonomies
  // 'lib/page-header-overwrites.php',        // Overwrite parent page header
  // 'lib/theme-functions.php',        // Theme-specific functions
  'lib/shortcodes.php',             // Theme-specific shortcodes
  // 'lib/meta/interview-meta.php',       // Meta Boxes for Packets
  // 'lib/widgets.php',           // Theme-specific widgets  
);

foreach ($bolt_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'bolt'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

?>
